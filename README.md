# AdventOfCode

[**Advent of Code**](https://adventofcode.com/) programming puzzles solver.

## Requirements

### Golang

Version 1.16

https://golang.org/doc/install

## Getting started

### Build & Run

    go build -o <bunary_name>
    ./<binary_name> -s <cookie_session_value>


Cookie session value you can define by dev tools from you web browser. [Login](https://adventofcode.com/2021/auth/login) and grub from any request cookie info.

### Usage

    NAME:
    adventofcode - Advent of Code solutions
    
    USAGE:
    adventofcode.exe [global options] command [command options] [arguments...]
    
    VERSION:
    v0.0.5
    
    COMMANDS:
    help, h  Shows a list of commands or help for one command
    
    GLOBAL OPTIONS:
    --session value, -s value  cookie session value
    --help, -h                 show help (default: false)
    --version, -v              print the version (default: false)


### Test

    cd ./tests
    go test -v


## puzzles coverage

- [**Day 1: Sonar Sweep**](https://adventofcode.com/2021/day/1) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day01.go)
- [**Day 2: Dive!**](https://adventofcode.com/2021/day/2) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day02.go)
- [**Day 3: Binary Diagnostic**](https://adventofcode.com/2021/day/3) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day03.go)
- [**Day 4: Giant Squid**](https://adventofcode.com/2021/day/4) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day04.go)
- [**Day 5: Hydrothermal Venture**](https://adventofcode.com/2021/day/5) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day05.go)
- [**Day 6: Lanternfish**](https://adventofcode.com/2021/day/6) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day06.go)
- [**Day 7: The Treachery of Whales**](https://adventofcode.com/2021/day/7) | [code](https://gitlab.com/gladtsinov/adventofcode/-/blob/main/pkg/events2021/day07.go)
