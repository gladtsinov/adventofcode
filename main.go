package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/urfave/cli/v2"
	"gitlab.com/gladtsinov/adventofcode/pkg"
	c "gitlab.com/gladtsinov/adventofcode/pkg/client"
	w "gitlab.com/gladtsinov/adventofcode/pkg/writer"
	"os"
	"sync"
	"time"
)

func main() {
	app := cli.App{
		Name:    "adventofcode",
		Usage:   "Advent of Code solutions",
		Version: version,
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     flagNameSession,
				Aliases:  []string{"s"},
				Usage:    "cookie session value",
				Required: true,
			},
		},
		Action: action,
	}

	err := app.Run(os.Args)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stdout, "application error: %v", err)
		_, _ = fmt.Fprintln(os.Stdout)
		os.Exit(1)
	}

	_, _ = fmt.Fprintln(os.Stdout)
	os.Exit(0)
}

func action(ctx *cli.Context) error {
	var events = pkg.Define2021()
	var puzzles = make([]*pkg.Puzzle, len(events))
	var wg = &sync.WaitGroup{}

	var report = pkg.Report{
		Puzzles: puzzles,
	}

	session := ctx.String(flagNameSession)
	client := c.NewClient(session)
	writer := w.NewMock()

	now := time.Now()
	for eventNumber, event := range events {
		wg.Add(1)

		event := event
		eventNumber := eventNumber

		go func() {
			defer wg.Done()

			puzzle := &pkg.Puzzle{
				Name:         event.Describe(),
				FirstAnswer:  nil,
				SecondAnswer: nil,
				Error:        nil,
			}

			now := time.Now()
			r1, r2, err := pkg.RunEvent(event, writer, client)

			if err != nil {
				puzzle.Error = err.Error()
			} else {
				puzzle.FirstAnswer = r1
				puzzle.SecondAnswer = r2
			}

			puzzle.Duration = fmt.Sprintf("%dms", time.Now().Sub(now).Milliseconds())

			report.Puzzles[eventNumber] = puzzle

		}()

	}

	wg.Wait()

	report.Duration = fmt.Sprintf("%dms", time.Now().Sub(now).Milliseconds())

	bReport, err := json.Marshal(report)
	if err != nil {
		return fmt.Errorf("can't encode report: %v", err)
	}

	var out bytes.Buffer
	err = json.Indent(&out, bReport, "", "    ")
	if err != nil {
		return fmt.Errorf("can't ident report: %v", err)
	}

	_, err = out.WriteTo(os.Stdout)
	if err != nil {
		return fmt.Errorf("can't write result")
	}

	return nil
}
