package tests

import (
	"fmt"
	"os"
	"testing"
)

var pointNames = []string{
	day01Name,
	day02Name,
	day03Name,
	day04Name,
	day05Name,
	day06Name,
	day07Name,
}

func TestMain(m *testing.M) {
	prepare()

	contentMap = make(map[string][]byte)

	points := definePoints(pointNames)

	for _, p := range points {
		content, err := box.ReadFile(p.path)
		if err != nil {
			fmt.Printf("GROUP TESTS FAIL %v\n", fmt.Errorf("can't read file '%s': %v", p.name, err))
			os.Exit(0)
		}

		contentMap[p.name] = content

	}

	os.Exit(m.Run())

}
