package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day06Name = "day06"

func TestDay06(t *testing.T) {
	event := events2021.NewDay06()
	event.SetInputPath(day06Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 377263 {
		t.Fatal("incorrect result1")
	}

	if result2 != 1695929023803 {
		t.Fatal("incorrect result2")
	}

}
