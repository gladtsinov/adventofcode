package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day05Name = "day05"

func TestDay05(t *testing.T) {
	event := events2021.NewDay05()
	event.SetInputPath(day05Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 8350 {
		t.Fatal("incorrect result1")
	}

	if result2 != 19374 {
		t.Fatal("incorrect result2")
	}

}
