package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day02Name = "day02"

func TestDay02(t *testing.T) {
	event := events2021.NewDay02()
	event.SetInputPath(day02Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 2036120 {
		t.Fatal("incorrect result1")
	}

	if result2 != 2015547716 {
		t.Fatal("incorrect result2")
	}

}
