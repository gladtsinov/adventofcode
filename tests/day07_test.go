package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day07Name = "day07"

func TestDay07(t *testing.T) {
	event := events2021.NewDay07()
	event.SetInputPath(day07Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 336131 {
		t.Fatal("incorrect result1")
	}

	if result2 != 92676646 {
		t.Fatal("incorrect result2")
	}

}
