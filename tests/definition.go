package tests

import (
	"embed"
	"fmt"
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	w "gitlab.com/gladtsinov/adventofcode/pkg/writer"
)

//go:embed data/*
var box embed.FS

var (
	client events2021.IClient
	writer pkg.Writer
)

func prepare() {
	client = new(mockClient)
	writer = w.NewMock()
}

type point struct {
	name string
	path string
}

func definePoints(names []string) []*point {
	var points []*point
	for _, name := range names {
		points = append(points, &point{
			name: name,
			path: fmt.Sprintf("data/%s.txt", name),
		})
	}

	return points
}

var contentMap map[string][]byte

type mockClient struct{}

func (c *mockClient) GetResponseBytes(path string) ([]byte, error) {
	content, ok := contentMap[path]
	if !ok {
		return nil, fmt.Errorf("unexpected content for '%s'", path)
	}

	return content, nil
}
