package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day03Name = "day03"

func TestDay03(t *testing.T) {
	event := events2021.NewDay03()
	event.SetInputPath(day03Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 2972336 {
		t.Fatal("incorrect result1")
	}

	if result2 != 3368358 {
		t.Fatal("incorrect result2")
	}

}
