package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day01Name = "day01"

func TestDay01(t *testing.T) {
	event := events2021.NewDay01()
	event.SetInputPath(day01Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 1233 {
		t.Fatal("incorrect result1")
	}

	if result2 != 1275 {
		t.Fatal("incorrect result2")
	}

}
