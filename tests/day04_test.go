package tests

import (
	"gitlab.com/gladtsinov/adventofcode/pkg"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"testing"
)

const day04Name = "day04"

func TestDay04(t *testing.T) {
	event := events2021.NewDay04()
	event.SetInputPath(day04Name)

	result1, result2, err := pkg.RunEvent(event, writer, client)
	if err != nil {
		t.Fatal(err)
	}

	if result1 != 8442 {
		t.Fatal("incorrect result1")
	}

	if result2 != 4590 {
		t.Fatal("incorrect result2")
	}

}
