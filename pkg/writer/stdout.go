package writer

import (
	"fmt"
	"os"
)

type StdOut struct {
	out *os.File
}

func NewStdOut() *StdOut {
	inst := StdOut{
		out: os.Stdout,
	}

	return &inst
}

func (s *StdOut) Write(layout string, parameters ...interface{}) {
	outString := fmt.Sprintf(layout, parameters...)
	outString += "\n"
	_, err := s.out.WriteString(outString)
	if err != nil {
		panic(err)
	}
}
