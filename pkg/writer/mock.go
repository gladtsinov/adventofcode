package writer

type mockWriter struct{}

func (w *mockWriter) Write(_ string, _ ...interface{}) {

}

func NewMock() *mockWriter {
	return &mockWriter{}
}
