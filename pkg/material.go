package pkg

type Puzzle struct {
	Name         string      `json:"name"`
	FirstAnswer  interface{} `json:"first_answer,omitempty"`
	SecondAnswer interface{} `json:"second_answer,omitempty"`
	Error        interface{} `json:"error,omitempty"`
	Duration     string      `json:"duration"`
}

type Report struct {
	Puzzles  []*Puzzle `json:"puzzles,omitempty"`
	Duration string    `json:"duration"`
}
