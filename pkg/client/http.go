package client

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const baseUrl = "https://adventofcode.com"

type Client struct {
	session string

	response *http.Response
}

func NewClient(session string) *Client {
	return &Client{
		session: session,
	}

}

func (inst *Client) get(path string) error {
	urlObject, _ := url.Parse(baseUrl)
	urlObject.Path = path

	request, err := http.NewRequest(http.MethodGet, urlObject.String(), nil)
	if err != nil {
		return fmt.Errorf("can't create request: %v", err)
	}

	request.AddCookie(&http.Cookie{
		Name:  "session",
		Value: inst.session,
	})

	inst.response, err = http.DefaultClient.Do(request)
	if err != nil {
		return fmt.Errorf("can't get responce: %v", err)
	}

	if inst.response.StatusCode != 200 {
		return fmt.Errorf("'%v' response status code", inst.response.Status)
	}

	return nil
}

func (inst *Client) responseBytes() ([]byte, error) {
	if inst.response == nil {
		return nil, fmt.Errorf("empty response")
	}

	raw, err := ioutil.ReadAll(inst.response.Body)
	if err != nil {
		return nil, fmt.Errorf("can't read http response: %v", err)
	}

	return raw, nil
}

func (inst *Client) GetResponseBytes(path string) ([]byte, error) {
	err := inst.get(path)
	if err != nil {
		return nil, fmt.Errorf("can't get input data: %v", err)
	}

	inputBytes, err := inst.responseBytes()
	if err != nil {
		return nil, fmt.Errorf("can't get response bytes: %v", err)
	}

	return inputBytes, nil
}
