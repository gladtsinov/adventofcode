package pkg

import (
	"fmt"
	"gitlab.com/gladtsinov/adventofcode/pkg/events2021"
	"time"
)

type Writer interface {
	Write(layout string, parameters ...interface{})
}

type IDescriber interface {
	Describe() string
}

type IGetStars interface {
	FirstStar() (interface{}, error)
	SecondStar() (interface{}, error)
}

type IData interface {
	GetData(c events2021.IClient) error
}

type Event interface {
	IData
	IDescriber
	IGetStars
}

func wrapDuration(w Writer, step string, f func() error) func() error {

	return func() error {
		now := time.Now()
		w.Write("step '%s' started", step)

		// call target function
		err := f()
		if err != nil {
			return fmt.Errorf("step '%s' failed: %v", step, err)
		}

		w.Write("step '%s' success; duration: %v\n", step, time.Now().Sub(now))

		return nil
	}
}

type Pipeline []func() error

func (p Pipeline) run() error {
	for _, step := range p {
		err := step()
		if err != nil {
			return err
		}
	}

	return nil
}

func RunEvent(e Event, w Writer, c events2021.IClient) (interface{}, interface{}, error) {
	now := time.Now()

	var result1 interface{}
	var result2 interface{}

	w.Write("--- %s ---", e.Describe())

	var pipeline Pipeline
	// define get data step
	pipeline = append(pipeline, wrapDuration(w, "get data", func() error {
		err := e.GetData(c)
		return err
	}))

	// define first solution calculation step
	pipeline = append(pipeline, wrapDuration(w, "first star", func() error {
		result, err := e.FirstStar()
		if err != nil {
			return err
		}

		w.Write("first result ---> %v", result)
		result1 = result

		return nil
	}))

	// define second solution calculation step
	pipeline = append(pipeline, wrapDuration(w, "second star", func() error {
		result, err := e.SecondStar()
		if err != nil {
			return err
		}

		w.Write("second result ---> %v", result)
		result2 = result

		return nil
	}))

	// run pipeline
	err := pipeline.run()
	if err != nil {
		return nil, nil, err
	}

	w.Write("--- '%s' --- complete; duration: %v\n", e.Describe(), time.Now().Sub(now))

	return result1, result2, nil
}
