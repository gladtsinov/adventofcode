package events2021

import (
	"fmt"
	"strconv"
	"strings"
)

type points [2]*point

func (p points) consider() ([]*point, bool) {
	isHorizontal := p[0].y == p[1].y
	isVertical := p[0].x == p[1].x

	var result []*point
	var start, end int
	var ok = true
	if isHorizontal {
		start, end = p[0].x, p[1].x

		if p[0].x > p[1].x {
			start, end = p[1].x, p[0].x
		}

		for x := start; x <= end; x++ {
			result = append(result, &point{
				x: x,
				y: p[0].y,
			})
		}
	} else if isVertical {
		start, end = p[0].y, p[1].y

		if p[0].y > p[1].y {
			start, end = p[1].y, p[0].y
		}

		for y := start; y <= end; y++ {
			result = append(result, &point{
				x: p[0].x,
				y: y,
			})
		}
	} else {
		ok = false
	}

	return result, ok
}

func (p points) diagonal() ([]*point, bool) {
	p0 := p[0]
	p1 := p[1]

	diffX := p0.x - p1.x
	diffY := p0.y - p1.y

	var result []*point

	var isDiagonal = diffX*diffX == diffY*diffY
	if isDiagonal {
		startX, endX := p0.x, p1.x
		y := p0.y

		if startX < endX {
			for x := startX; x <= endX; x++ {

				result = append(result, &point{
					x: x,
					y: y,
				})

				if y < p1.y {
					y++
				} else {
					y--
				}
			}
		} else {
			for x := startX; x >= endX; x-- {

				result = append(result, &point{
					x: x,
					y: y,
				})

				if y < p1.y {
					y++
				} else {
					y--
				}
			}
		}

		return result, true
	}

	return result, false
}

type point struct {
	x, y int
}

func newPoint(s string) (*point, error) {
	var point = new(point)
	var firstGrab = false
	for _, sDigit := range strings.Split(s, ",") {
		digit, err := strconv.Atoi(sDigit)
		if err != nil {
			return nil, fmt.Errorf("can't define point number '%s' as int: %v", sDigit, err)
		}

		if !firstGrab {
			point.x = digit
			firstGrab = true
		} else {
			point.y = digit
		}

	}

	return point, nil
}

type day05 struct {
	input     map[point]int
	diagonals []*point

	result int

	describer
	data
	getterStars
}

func NewDay05() *day05 {
	const (
		day         = 5
		description = "Day 5: Hydrothermal Venture"
	)

	var inst day05

	inst.input = make(map[point]int)

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		next()

		return inst.result, nil
	})

	inst.setFirstStar(func() {
		for _, value := range inst.input {
			if value > 1 {
				inst.result++
			}
		}
	})

	inst.setSecondStar(func() {
		inst.result = 0

		for _, point := range inst.diagonals {
			_, found := inst.input[*point]
			if found {
				inst.input[*point]++
			} else {
				inst.input[*point] = 1
			}
		}

		inst.firstStar()

	})

	inst.setParser(func(row string) error {
		sPoints := strings.Split(row, " -> ")
		if len(sPoints) != 2 {
			return fmt.Errorf("unexpected input row '%s'", row)
		}

		var points points
		var err error
		points[0], err = newPoint(sPoints[0])
		if err != nil {
			return fmt.Errorf("can't define start point")
		}

		points[1], err = newPoint(sPoints[1])
		if err != nil {
			return fmt.Errorf("can't define end point")
		}

		pointList, ok := points.consider()
		if ok {
			for _, p := range pointList {
				_, found := inst.input[*p]
				if found {
					inst.input[*p]++
				} else {
					inst.input[*p] = 1
				}
			}
		} else {
			dPoints, ok := points.diagonal()
			if ok {
				inst.diagonals = append(inst.diagonals, dPoints...)
			}
		}

		return nil
	})

	return &inst
}
