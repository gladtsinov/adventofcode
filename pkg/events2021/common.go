package events2021

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
)

const pathLayout = "/2021/day/%d/input"

type simpleCallback func()
type simpleWrapperWithError func(simpleCallback) (interface{}, error)
type stringCallbackWithError func(string) error

func definePath(day int) string {

	return fmt.Sprintf(pathLayout, day)
}

type describer struct {
	description string
}

func (inst *describer) Describe() string {
	return inst.description
}

func (inst *describer) setDescription(description string) {

	inst.description = description
}

type getter struct {
	inputPath string
}

func (inst *getter) SetInputPath(inputPath string) {

	inst.inputPath = inputPath
}

type IClient interface {
	GetResponseBytes(path string) ([]byte, error)
}

func (inst *getter) getData(client IClient, callback stringCallbackWithError) error {
	inputBytes, err := client.GetResponseBytes(inst.inputPath)
	if err != nil {
		return fmt.Errorf("can't get input byte data: %v", err)
	}

	reader := bufio.NewReader(bytes.NewReader(inputBytes))
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return fmt.Errorf("can't read data: %v", err)
			}
		}

		row := string(line)
		err = callback(row)
		if err != nil {
			return fmt.Errorf("can't parce row '%s': %v", row, err)
		}
	}

	return nil
}

type getterStars struct {
	firstStar  simpleCallback
	secondStar simpleCallback
	wrapper    simpleWrapperWithError
}

func (inst *getterStars) setFirstStar(c simpleCallback) {

	inst.firstStar = c
}

func (inst *getterStars) setSecondStar(c simpleCallback) {

	inst.secondStar = c
}

func (inst *getterStars) setWrapper(w simpleWrapperWithError) {

	inst.wrapper = w
}

func (inst *getterStars) FirstStar() (interface{}, error) {

	return inst.wrapper(inst.firstStar)
}

func (inst *getterStars) SecondStar() (interface{}, error) {

	return inst.wrapper(inst.secondStar)
}

type data struct {
	parser stringCallbackWithError

	getter
}

func (inst *data) setParser(c stringCallbackWithError) {

	inst.parser = c
}

func (inst *data) GetData(c IClient) error {
	err := inst.getData(c, inst.parser)
	if err != nil {
		return fmt.Errorf("can't get data: %v", err)
	}

	return nil
}
