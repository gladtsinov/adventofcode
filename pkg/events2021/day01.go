package events2021

import (
	"fmt"
	"strconv"
)

type day01 struct {
	input []int

	increased int
	decreased int

	describer
	data
	getterStars
}

func NewDay01() *day01 {
	const (
		day         = 1
		description = "Day 1: Sonar Sweep"
	)

	var inst day01

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		inst.increased = 0
		inst.decreased = 0

		next()

		return inst.max(), nil
	})

	inst.setFirstStar(func() {
		for i := 1; i < len(inst.input); i++ {
			if inst.input[i] < inst.input[i-1] {
				inst.increased++
			} else {
				inst.decreased++
			}
		}
	})

	inst.setSecondStar(func() {
		for i := 3; i < len(inst.input); i++ {
			if inst.input[i] > inst.input[i-3] {
				inst.increased++
			} else if inst.input[i] < inst.input[i-3] {
				inst.decreased++
			}
		}
	})

	inst.setParser(func(row string) error {
		digit, err := strconv.Atoi(row)
		if err != nil {
			return fmt.Errorf("can't decode '%s' to int: %v", row, err)
		}

		inst.input = append(inst.input, digit)

		return nil
	})

	return &inst
}

func (inst *day01) max() int {
	if inst.increased > inst.decreased {
		return inst.increased
	} else {
		return inst.decreased
	}
}
