package events2021

import (
	"fmt"
	"math"
	"sync"
)

type day03 struct {
	input    [][]string
	rowCount int

	x, y int

	describer
	data
	getterStars
}

func NewDay03() *day03 {
	const (
		day         = 3
		description = "Day 3: Binary Diagnostic"
	)

	var inst day03

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	inst.input = make([][]string, 0)
	inst.rowCount = 0

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		inst.x = 0
		inst.y = 0

		next()

		return inst.x * inst.y, nil
	})

	inst.setFirstStar(func() {
		const zero = "0"
		var parsed = make(map[int]int)
		for _, sDigits := range inst.input {
			for j, sDigit := range sDigits {
				if sDigit == zero {
					parsed[j]++
				}
			}
		}

		var bits []bool
		for position := 0; position < len(parsed); position++ {
			zeros := parsed[position]
			if zeros > (inst.rowCount / 2) {
				bits = append(bits, false)
			} else {
				bits = append(bits, true)
			}
		}

		lastIndex := len(bits) - 1
		for i, bit := range bits {
			var x = 0
			var oppositeX = 1
			if bit {
				x = 1
				oppositeX = 0
			}

			var y = float64(lastIndex - i)

			pow := int(math.Pow(2, y))

			inst.x += x * pow
			inst.y += oppositeX * pow
		}

	})

	inst.setSecondStar(func() {
		var wg sync.WaitGroup
		for _, isOxygen := range []bool{true, false} {
			wg.Add(1)

			isOxygen := isOxygen
			index := 0
			go func() {
				defer wg.Done()

				inst.determine(inst.input, index, isOxygen)
			}()
		}

		wg.Wait()
	})

	inst.setParser(func(row string) error {
		var sStrings []string
		for _, rDigit := range row {
			sStrings = append(sStrings, string(rDigit))
		}

		inst.input = append(inst.input, sStrings)
		inst.rowCount++

		return nil
	})

	return &inst
}

func (inst *day03) determine(data [][]string, index int, isOxygen bool) {
	const zero = "0"

	lenData := len(data)
	if lenData == 0 {
		return
	}

	if lenData == 1 {
		lastIndex := len(data[0]) - 1
		for i, sBit := range data[0] {
			var x = 0
			if sBit != zero {
				x = 1
			}

			var y = float64(lastIndex - i)

			pow := int(math.Pow(2, y))

			if isOxygen {
				inst.x += x * pow
			} else {
				inst.y += x * pow

			}
		}

		return
	}

	var count int
	var zeroData [][]string
	var nonZeroData [][]string
	var lenDataItem = len(data[0])

	for _, items := range data {
		if items[index] == zero {
			zeroData = append(zeroData, items)
			count++
		} else {
			nonZeroData = append(nonZeroData, items)
		}
	}

	isZeroMore := count > (lenData / 2)

	index++
	if index == lenDataItem {
		// delete duplicates
		nonZeroData = nonZeroData[:1]
		zeroData = zeroData[:1]
	}

	if isOxygen {
		if isZeroMore {
			inst.determine(zeroData, index, isOxygen)
		} else {
			inst.determine(nonZeroData, index, isOxygen)
		}
	} else {
		if isZeroMore {
			inst.determine(nonZeroData, index, isOxygen)
		} else {
			inst.determine(zeroData, index, isOxygen)
		}
	}
}
