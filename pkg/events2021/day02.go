package events2021

import (
	"fmt"
	"strconv"
	"strings"
)

type driveCommand struct {
	direction string
	units     int
}

type day02 struct {
	input []*driveCommand

	x   int
	y   int
	aim int

	directionForward string
	directionUp      string

	describer
	data
	getterStars
}

func NewDay02() *day02 {
	const (
		day         = 2
		description = "Day 2: Dive!"
	)

	var inst day02

	inst.directionForward = "forward"
	inst.directionUp = "up"

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		inst.x = 0
		inst.y = 0
		inst.aim = 0

		next()

		return inst.x * inst.y, nil
	})

	inst.setFirstStar(func() {
		for _, command := range inst.input {
			if command.direction == inst.directionForward {
				inst.x += command.units
			} else {
				if command.direction == inst.directionUp {
					inst.y -= command.units
				} else {
					inst.y += command.units
				}
			}
		}
	})

	inst.setSecondStar(func() {
		for _, command := range inst.input {
			if command.direction == inst.directionForward {
				inst.x += command.units
				inst.y += inst.aim * command.units
			} else {
				if command.direction == inst.directionUp {
					inst.aim -= command.units
				} else {
					inst.aim += command.units
				}
			}
		}
	})

	inst.setParser(func(row string) error {
		command := strings.Split(row, " ")
		units, err := strconv.Atoi(command[1])
		if err != nil {
			return fmt.Errorf("can't decode '%s' to int: %v", command[1], err)
		}

		inst.input = append(inst.input, &driveCommand{
			direction: command[0],
			units:     units,
		})

		return nil
	})

	return &inst
}
