package events2021

import (
	"fmt"
	"strconv"
	"strings"
)

type day06 struct {
	input map[int]int

	describer
	data
	getterStars
}

func NewDay06() *day06 {
	const (
		day         = 6
		description = "Day 6: Lanternfish"
	)

	var inst day06

	inst.input = make(map[int]int)

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		next()

		var result int
		for _, value := range inst.input {
			result += value
		}

		return result, nil
	})

	inst.setFirstStar(func() {
		inst.calc(0, 80)
	})

	inst.setSecondStar(func() {
		inst.calc(80, 256)
	})

	inst.setParser(func(row string) error {
		for _, sDigit := range strings.Split(row, ",") {
			digit, err := strconv.Atoi(sDigit)
			if err != nil {
				return fmt.Errorf("can't define number '%s' as int: %v", sDigit, err)
			}

			inst.input[digit]++
		}

		return nil
	})

	return &inst
}

func (inst day06) calc(from, to int) {

	for i := from; i < to; i++ {
		newborns, wasBorn := inst.input[0]
		if wasBorn {
			inst.input[7] += newborns
		}

		for j := 1; j < 9; j++ {
			inst.input[j-1] = inst.input[j]
		}

		if wasBorn {
			inst.input[8] = newborns
		}
	}
}
