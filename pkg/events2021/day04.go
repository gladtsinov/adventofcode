package events2021

import (
	"fmt"
	"strconv"
	"strings"
)

type board struct {
	sum          int
	rowNumber    int
	columnNumber int

	won bool

	rows    []*subsequence
	columns []*subsequence
}

type subsequence struct {
	isRow   bool
	pBoard  *board
	sum     int
	remains int
}

func (s *subsequence) drawn(number int) (isReady bool) {
	//s.sum += number
	s.remains--
	if s.isRow {
		s.pBoard.sum -= number
	}

	if s.remains == 0 {

		return true
	}

	return false
}

func (s *subsequence) numbers() (x int, y int) {
	return s.sum, s.pBoard.sum
}

type day04 struct {
	numbers []int
	defined bool

	lastPosition int
	boardsCount  int

	boards    []*board
	numberMap map[int][]*subsequence

	x int
	y int

	describer
	data
	getterStars
}

func NewDay04() *day04 {
	const (
		day         = 4
		description = "Day 4: Giant Squid"
	)

	var inst day04

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	inst.boards = make([]*board, 0)
	inst.defined = false
	inst.numberMap = make(map[int][]*subsequence)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.numbers == nil {
			return nil, fmt.Errorf("empty bingo numbers")
		}

		inst.x = 0
		inst.y = 0

		next()

		return inst.x * inst.y, nil
	})

	inst.setFirstStar(func() {
		for i, number := range inst.numbers {
			for _, seq := range inst.numberMap[number] {
				inst.lastPosition = i

				isReady := seq.drawn(number)
				if isReady {
					inst.boardsCount--
					seq.pBoard.won = true

					inst.x = number
					inst.y = seq.pBoard.sum

					return
				}
			}

		}

	})

	inst.setSecondStar(func() {
		for i := inst.lastPosition + 1; i < len(inst.numbers); i++ {
			number := inst.numbers[i]
			for _, seq := range inst.numberMap[number] {
				isReady := seq.drawn(number)
				if isReady {
					if !seq.pBoard.won {
						inst.boardsCount--
						seq.pBoard.won = true
					}

					if inst.boardsCount == 0 {
						inst.x = number
						inst.y = seq.pBoard.sum

						return
					}
				}
			}
		}
	})

	inst.setParser(func(row string) error {
		if inst.defined == false {
			for _, sNumber := range strings.Split(row, ",") {
				number, err := strconv.Atoi(sNumber)
				if err != nil {
					return fmt.Errorf("can't define number '%s' as int: %v", sNumber, err)
				}

				inst.numbers = append(inst.numbers, number)
			}

			inst.defined = true
			return nil
		}

		if row == "" {
			inst.boardsCount++

			inst.boards = append(inst.boards, &board{
				sum:          0,
				rowNumber:    0,
				columnNumber: 0,
				rows:         make([]*subsequence, 0),
				columns:      make([]*subsequence, 0),
			})

			return nil
		}

		board, err := inst.currentBoard()
		if err != nil {
			return fmt.Errorf("can't define current board")
		}

		for _, sDigit := range strings.Split(row, " ") {
			if sDigit == "" {
				continue
			}

			digit, err := strconv.Atoi(sDigit)
			if err != nil {
				return fmt.Errorf("can't define board number '%s' as int: %v", sDigit, err)
			}

			row := board.getRow()
			row.remains++
			row.sum += digit

			column := board.getColumn()
			column.remains++
			column.sum += digit

			inst.numberMap[digit] = append(inst.numberMap[digit], row, column)

			board.sum += digit

			board.columnNumber++
		}

		board.rowNumber++
		board.columnNumber = 0

		return nil
	})

	return &inst
}

func (inst *day04) currentBoard() (*board, error) {
	lenBoards := len(inst.boards)
	if lenBoards == 0 {
		return nil, fmt.Errorf("empty board list")
	}

	return inst.boards[lenBoards-1], nil
}

func (inst *board) getRow() *subsequence {
	if inst.rowNumber == len(inst.rows) {
		row := subsequence{
			isRow:   true,
			pBoard:  inst,
			sum:     0,
			remains: 0,
		}

		inst.rows = append(inst.rows, &row)
	}

	return inst.rows[inst.rowNumber]
}

func (inst *board) getColumn() *subsequence {
	if inst.columnNumber == len(inst.columns) {
		column := subsequence{
			isRow:   false,
			pBoard:  inst,
			sum:     0,
			remains: 0,
		}

		inst.columns = append(inst.columns, &column)
	}

	return inst.columns[inst.columnNumber]
}
