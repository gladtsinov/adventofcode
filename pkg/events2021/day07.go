package events2021

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type day07 struct {
	input    []int
	inputMap map[int]int

	mean        int
	inputLength int
	result      int
	result02    int

	describer
	data
	getterStars
}

func NewDay07() *day07 {
	const (
		day         = 7
		description = "Day 7: The Treachery of Whales"
	)

	var inst day07

	inst.SetInputPath(definePath(day))
	inst.setDescription(description)

	inst.inputMap = make(map[int]int)

	// callbacks
	inst.setWrapper(func(next simpleCallback) (interface{}, error) {
		if inst.input == nil {
			return nil, fmt.Errorf("empty input data")
		}

		next()

		return inst.result, nil
	})

	inst.setFirstStar(func() {
		median := inst.input[inst.inputLength/2]
		for digit, multiply := range inst.inputMap {
			// part 01
			fuel := digit - median
			if fuel < 0 {
				fuel *= -1
			}

			inst.result += fuel * multiply

			// part 02
			abs := digit - inst.mean
			if abs < 0 {
				abs *= -1
			}

			inst.result02 += (abs * (1 + abs) / 2) * multiply
		}
	})

	inst.setSecondStar(func() {
		inst.result = inst.result02
	})

	inst.setParser(func(row string) error {
		var sum int
		for _, sDigit := range strings.Split(row, ",") {
			digit, err := strconv.Atoi(sDigit)
			if err != nil {
				return fmt.Errorf("can't decode '%s' to int: %v", sDigit, err)
			}

			inst.input = append(inst.input, digit)
			inst.inputMap[digit]++

			sum += digit
			inst.inputLength++
		}

		sort.Ints(inst.input)

		inst.mean = sum / inst.inputLength

		return nil
	})

	return &inst
}
