package pkg

import (
	events20212 "gitlab.com/gladtsinov/adventofcode/pkg/events2021"
)

type Events []Event

func Define2021() Events {
	return []Event{
		events20212.NewDay01(),
		events20212.NewDay02(),
		events20212.NewDay03(),
		events20212.NewDay04(),
		events20212.NewDay05(),
		events20212.NewDay06(),
		events20212.NewDay07(),
	}
}
